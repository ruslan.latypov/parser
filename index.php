<?php
require "vendor/autoload.php";
use PHPHtmlParser\Dom;

function substrwords($text, $maxchar, $end='...') {
  if (strlen($text) > $maxchar || $text == '') {
    $words = preg_split('/\s/', $text);
    $output = '';
    $i = 0;
    while (1) {
      $length = strlen($output) + strlen($words[$i]);
      if ($length > $maxchar) {
        break;
      }
      else {
        $output .= " " . $words[$i];
        ++$i;
      }
    }
    $output .= $end;
  }
  else {
    $output = $text;
  }
  return $output;
}

function parse () {
  $dom = new Dom;
  $dom->loadFromUrl('https://www.rbc.ru/');
  $newsListItem = $dom->find('.js-news-feed-list .news-feed__item');
  $newsListItemLength = count($newsListItem);
  $newsList = '';
  $newsPage = '';

  for ($i = 0; $i <= $newsListItemLength; $i++) {
    if ($newsListItem[$i]) {
      $newsListLink = $newsListItem[$i]->getAttribute('href');
      $dom->loadFromUrl($newsListLink);
      if ($dom->find('.article.js-rbcslider-article')[0]) {
        $newsDescription = $dom->find('.article')[0]->find('.article__text');
        $newsPage .= '<div class="news-page news-page-' . $i . '">' . $dom->find('.article')[0]->find('.article__header__title') . $newsDescription . '</div>';
        $newsListItem[$i]->setAttribute('href', '?id=' . $i);
        $newsListDescription = '<div><p>' . substrwords(strip_tags($newsDescription), 200) . '</p><p><a href="?id=' . $i . '">Подробнее</a></p></div>';
        $newsList .= '<li class="news__item news__item--' . $i . '">' . $newsListItem[$i] . $newsListDescription . '</li>';
      }
    }
  }

  $newsList = '<ul class="news">' . $newsList . '</ul>';
  file_put_contents('news.txt', $newsList . $newsPage);
}

function showContent () {
  $dom = new Dom;
  $dom->loadFromFile('news.txt');

  if (isset($_GET['id'])) {
    $contents = $dom->find('.news-page-' . $_GET['id']);
  } else {
    $contents = $dom->find('.news');
  }

  echo $contents;
}

if (isset($_GET['parse'])) {
  parse();
}

showContent();
?>
